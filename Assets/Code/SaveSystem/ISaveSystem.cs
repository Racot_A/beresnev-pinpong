﻿public interface ISaveSystem
{
    void SaveData();
    void LoadData();

    void AddData(string key, string value);
    string GetData(string key);
    void RemoveData(string key);
}
