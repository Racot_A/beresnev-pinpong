﻿using System.IO;
using UnityEngine;

public class PersistentStorageSaveSystem : MonoBehaviour, ISaveSystem
{
    [SerializeField] private SaveDataContainer _saveDataContainer;
    
    private string SavePath => Application.persistentDataPath + "/" + "LocalSaves.json";
    
    public void SaveData()
    {
        DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);
        if (!di.Exists)
        {
            Debug.LogError("Save Directory NOT Exist");
            return;
        }
        
        var jsonSaveDataText = JsonUtility.ToJson(_saveDataContainer, true);
        File.WriteAllText(SavePath, jsonSaveDataText);
        
        Debug.LogWarning($"[Save] Data:{jsonSaveDataText}");
    }

    public void LoadData()
    {
        DirectoryInfo di = new DirectoryInfo(Application.persistentDataPath);
        if (!di.Exists || !File.Exists(SavePath))
        {
            Debug.LogError("Load Directory NOT Exist");
            return;
        }
        
        var jsonText = File.ReadAllText(SavePath);
        
        if(string.IsNullOrEmpty(jsonText)) { return; }
        
        _saveDataContainer = JsonUtility.FromJson<SaveDataContainer>(jsonText);
    }

    public void AddData(string key, string value)
    {
        _saveDataContainer.Add(key, value);
    }

    public string GetData(string key)
    {
        return _saveDataContainer.Get(key);
    }

    public void RemoveData(string key)
    {
        _saveDataContainer.Remove(key);
    }
}
