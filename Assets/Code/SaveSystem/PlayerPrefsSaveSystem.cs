﻿using UnityEngine;

public class PlayerPrefsSaveSystem : MonoBehaviour, ISaveSystem
{
    [SerializeField] private string saveName = "pingPong";
    [SerializeField] private SaveDataContainer _saveDataContainer;
    
    public void SaveData()
    {
        var jsonSaveDataText = JsonUtility.ToJson(_saveDataContainer, true);
        Debug.LogWarning($"SaveData:{jsonSaveDataText}");
        PlayerPrefs.SetString(saveName, jsonSaveDataText);
        PlayerPrefs.Save();
    }

    public void LoadData()
    {
        var jsonText = PlayerPrefs.GetString(saveName);
        if(string.IsNullOrEmpty(jsonText)) { return; }
        Debug.LogWarning($"LoadData:{jsonText}");
        _saveDataContainer = JsonUtility.FromJson<SaveDataContainer>(jsonText);
    }

    public void AddData(string key, string value)
    {
        _saveDataContainer.Add(key, value);
    }

    public string GetData(string key)
    {
        return _saveDataContainer.Get(key);
    }

    public void RemoveData(string key)
    {
        _saveDataContainer.Remove(key);
    }
}
