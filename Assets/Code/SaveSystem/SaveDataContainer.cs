﻿using System;
using System.Collections.Generic;

[Serializable]
public class SaveDataContainer
{
    public List<SaveDataItem> SaveDataItems = new List<SaveDataItem>();

    public void Add(string key, string value)
    {
        if (HasKey(key))
        {
            foreach (var saveDataItem in SaveDataItems)
            {
                if (saveDataItem.Key.Equals(key))
                {
                    saveDataItem.Value = value;
                    break;
                }
            }
        }
        else
        {
            SaveDataItems.Add(new SaveDataItem(key, value));
        }
    }
    
    public string Get(string key)
    {
        if (HasKey(key))
        {
            return SaveDataItems.Find(x => x.Key.Equals(key)).Value;
        }
        return string.Empty;
    }
    
    public void Remove(string key)
    {
        if (!HasKey(key)) { return; }

        var index = SaveDataItems.FindIndex(x => x.Key.Equals(key));
        SaveDataItems.RemoveAt(index);
    }

    public bool HasKey(string key)
    {
        return SaveDataItems.Find(x => x.Key.Equals(key)) != null;
    }
    
}
