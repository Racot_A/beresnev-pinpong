﻿using System;

[Serializable]
public class SaveDataItem
{
    public string Key;
    public string Value;

    public SaveDataItem(string key, string value)
    {
        Key = key;
        Value = value;
    }
}
