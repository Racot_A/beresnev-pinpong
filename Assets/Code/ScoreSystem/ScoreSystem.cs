﻿using System;
using UnityEngine;

public class ScoreSystem : MonoBehaviour
{
    public event Action<int> ScoreUpdate;
    public event Action<int> BestScoreUpdate;

    public const string BestScoreSaveId = "BestScore";
    
    public int Score
    {
        get { return currentScore; }
        set
        {
            currentScore = value;

            if (currentScore >= bestScore)
            {
                bestScore = currentScore;
                BestScoreUpdate?.Invoke(bestScore);

                SaveScore();
            }
            ScoreUpdate?.Invoke(currentScore);
        }
    }
    
    public int BestScore => bestScore;

    [SerializeField] private int currentScore;
    [SerializeField] private int bestScore;

    public void SaveScore()
    {
        var saveData = new SaveScoreData()
        {
            BestScore = bestScore
        };

        var jsonSaveScoreData = JsonUtility.ToJson(saveData, true);
        Debug.LogWarning($"Save Score Data:{jsonSaveScoreData}");
        Core.Instance.SaveSystem.AddData(BestScoreSaveId, jsonSaveScoreData);
        
        Core.Instance.SaveSystem.SaveData();
    }

    public void LoadScore()
    {
        var jsonData = Core.Instance.SaveSystem.GetData(BestScoreSaveId);

        if (string.IsNullOrEmpty(jsonData)) { return; }
        
        Debug.LogWarning($"Load Score Data:{jsonData}");
        var saveScoreData = JsonUtility.FromJson<SaveScoreData>(jsonData);

        bestScore = saveScoreData.BestScore;
        BestScoreUpdate?.Invoke(bestScore);

        ResetSessionScore();
    }

    public void ResetSessionScore()
    {
        Score = 0;
    }
}
