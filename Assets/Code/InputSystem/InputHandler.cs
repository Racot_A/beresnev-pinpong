﻿using System;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    public event Action<Vector2> MoveEvent;

    private Vector2 _input;

    private void Update()
    {
        HandleInput();
    }

    private void HandleInput()
    {
        if (Application.isMobilePlatform)
        {
            HandleMobileInput();
        }
        else
        {
            HandlePCInput();
        }
    }
    private void HandlePCInput()
    {
        _input.x = Input.GetAxis("Horizontal");
        MoveEvent?.Invoke(_input);
    }

    private void HandleMobileInput()
    {
        _input.x = Input.GetAxis("Mouse X");
        MoveEvent?.Invoke(_input);
    }
}
