﻿using UnityEngine;

public static class VectorExtentions
{
    public static Vector2 ToRaw(this Vector2 vector2)
    {
        return new Vector2(vector2.x.ToRaw(), vector2.y.ToRaw());
    }

    public static float ToRaw(this float value)
    {
        if (value > 0)
        {
            return 1;
        }

        if (value < 0)
        {
            return -1;
        }

        return 0;
    }
}
