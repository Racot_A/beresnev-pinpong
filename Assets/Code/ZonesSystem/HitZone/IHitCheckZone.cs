﻿public interface IHitCheckZone
{
    void RegisterListener(IHitCheckZoneListener listener);
    
    void UnregisterListener(IHitCheckZoneListener listener);
}
