﻿using UnityEngine;

public interface IHitCheckZoneListener
{
    void OnHit(Collision2D collision);
}
