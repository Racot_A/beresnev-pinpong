﻿using System.Collections.Generic;
using UnityEngine;

public class ColliderHitZone : MonoBehaviour, IHitCheckZone
{
    private List<IHitCheckZoneListener> _listeners = new List<IHitCheckZoneListener>();
    
    public void RegisterListener(IHitCheckZoneListener listener)
    {
        _listeners.Add(listener);
    }

    public void UnregisterListener(IHitCheckZoneListener listener)
    {
        _listeners.Remove(listener);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        for (int i = _listeners.Count - 1; i >= 0; --i)
        {
            var listener = _listeners[i];
            listener.OnHit(other);
        }
    }
}
