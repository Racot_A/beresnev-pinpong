﻿using System;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class UnityBallvent : UnityEvent<Ball>{} 

public class BallTriggerZone : MonoBehaviour
{
    [SerializeField] private UnityBallvent ballEnterEvent;
    [SerializeField] private UnityBallvent ballExitEvent;
    public UnityBallvent BallEnterEvent => ballEnterEvent;
    public UnityBallvent BallExitEvent => ballExitEvent;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        var ball = other.GetComponentInParent<Ball>();
        if(!ball) { return; }
        
        ballEnterEvent?.Invoke(ball);
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        var ball = other.GetComponentInParent<Ball>();
        if(!ball) { return; }
        
        ballExitEvent?.Invoke(ball);
    }
}
