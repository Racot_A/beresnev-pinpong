﻿using System;
using UnityEngine;

[Serializable]
public class BallCustomizationData
{
    public string Id;
    public Color Color;

    public BallCustomizationData(string id, Color color)
    {
        Id = id;
        Color = color;
    }
}
