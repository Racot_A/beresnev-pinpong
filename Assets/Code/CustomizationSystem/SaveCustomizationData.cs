﻿using System;
using System.Collections.Generic;

[Serializable]
public class SaveCustomizationData
{
    public List<BallCustomizationData> SaveData = new List<BallCustomizationData>();
}
