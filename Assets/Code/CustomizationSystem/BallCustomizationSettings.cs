﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = nameof(BallCustomizationSettings), menuName = "ScriptableObjects/Customization/" + nameof(BallCustomizationSettings))]

public class BallCustomizationSettings : ScriptableObject
{
    public List<Color> AvailableColors => availableColors;
    
    [SerializeField] private List<Color> availableColors = new List<Color>(5);
}
