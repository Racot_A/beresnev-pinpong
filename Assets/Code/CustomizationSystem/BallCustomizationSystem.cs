﻿using System.Collections.Generic;
using UnityEngine;

public class BallCustomizationSystem : MonoBehaviour
{
    public BallCustomizationSettings CustomizationSettings => customizationSettings;
    
    [SerializeField] private string customizationSaveID = "customization";
    [SerializeField] private BallCustomizationSettings customizationSettings;
    
    [SerializeField] private List<BallCustomizationData> customizationData = new List<BallCustomizationData>();
    
    public void SetBallColor(string ballId, Color color)
    {
        var hasData = customizationData.Find(x => x.Id.Equals(ballId)) != null;

        if (hasData)
        {
            foreach (var data in customizationData)
            {
                if (data.Id.Equals(ballId))
                {
                    data.Color = color;
                    break;
                }
            }
        }
        else
        {
            customizationData.Add(new BallCustomizationData(ballId, color));
        }
        
        SaveData();
    }
    public Color GetBallColor(string ballId)
    {
        var data = customizationData.Find(x => x.Id.Equals(ballId));
        return data?.Color ?? Color.white;
    }

    public void SaveData()
    {
        var saveData = new SaveCustomizationData();
        saveData.SaveData = customizationData;

        var data = JsonUtility.ToJson(saveData, true);
        Debug.LogWarning($"Save Customization Data:{data}");
        Core.Instance.SaveSystem.AddData(customizationSaveID, data);
        
        //Save All Data
        Core.Instance.SaveSystem.SaveData();
    }

    public void LoadData()
    {
        var jsonData = Core.Instance.SaveSystem.GetData(customizationSaveID);

        if (string.IsNullOrEmpty(jsonData)) { return; }
        
        Debug.LogWarning($"Load Customization Data:{jsonData}");
        var loadedData = JsonUtility.FromJson<SaveCustomizationData>(jsonData);
        customizationData.Clear();
        customizationData = loadedData.SaveData;
    }
}
