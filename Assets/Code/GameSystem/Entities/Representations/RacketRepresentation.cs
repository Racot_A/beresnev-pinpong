﻿using UnityEngine;

[CreateAssetMenu(fileName = nameof(RacketRepresentation), menuName = "ScriptableObjects/Game/Entities/" + nameof(RacketRepresentation))]
public class RacketRepresentation : ScriptableObject
{
    public float DefaultSpeed => defaultSpeed;
    
    [SerializeField, Range(1, 100)] private float defaultSpeed = 10;
    [Space(10)]
    [SerializeField] private float minSpeed = 1;
    [SerializeField] private float maxSpeed = 100;


    public float GetRandomSpeed()
    {
        return UnityEngine.Random.Range(minSpeed, maxSpeed);
    }
}

