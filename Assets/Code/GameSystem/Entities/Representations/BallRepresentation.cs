﻿using UnityEngine;

[CreateAssetMenu(fileName = nameof(BallRepresentation), menuName = "ScriptableObjects/Game/Entities/" + nameof(BallRepresentation))]
public class BallRepresentation : ScriptableObject
{
    public string Id => id;
    public float DefaultSpeed => defaultSpeed;
    public Vector2 DefaultScale => defaultScale;

    [SerializeField] private string id;
    [Header("[Scale]")] 
    [SerializeField, Range(1, 100)] private float defaultSpeed = 10;
    [Space(10)]
    [SerializeField] private float minSpeed = 1;
    [SerializeField] private float maxSpeed = 100;
    [Header("[Scale]")] 
    [SerializeField] private Vector2 defaultScale = Vector2.zero;
    public float GetRandomSpeed()
    {
        return UnityEngine.Random.Range(minSpeed, maxSpeed);
    }
}
