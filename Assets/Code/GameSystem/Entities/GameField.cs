﻿using UnityEngine;

public class GameField : MonoBehaviour
{
    [SerializeField] private Transform centerPoint;

    public Vector3 GetCenter()
    {
        return centerPoint.position;
    }
}
