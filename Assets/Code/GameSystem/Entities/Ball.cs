﻿using UnityEngine;
using Random = UnityEngine.Random;

public class Ball : MonoBehaviour, IHitCheckZoneListener
{
    public BallRepresentation Representation => representation;
    
    [SerializeField] private BallRepresentation representation;
    [SerializeField] private SpriteRenderer ballRenderer;
    
    private float _moveSpeed;
    
    private Vector2 _moveDirection;
    private Rigidbody2D _rigidbody;
    private IHitCheckZone _hitCheckZone;

    private void Awake()
    {
        InitComponents();
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void InitComponents()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        
        _hitCheckZone = GetComponentInChildren<IHitCheckZone>();
        _hitCheckZone.RegisterListener(this);
        
        _moveSpeed = representation.DefaultSpeed;
        transform.localScale = representation.DefaultScale;
    }

    private void Move()
    {
        _rigidbody.velocity = _moveDirection * _moveSpeed;
    }

    public void StartMove()
    {
        _moveDirection = GetRandomDirection();
    }

    public void ResetToPosition(Vector2 pos)
    {
        _moveDirection = Vector2.zero;
        _rigidbody.velocity = Vector2.zero;
        transform.position = pos;
    }

    private Vector2 GetRandomDirection()
    {
        return Random.insideUnitCircle.normalized;
    }

    public void OnHit(Collision2D collision)
    {
        //Add Score if we touch racket
        var racket = collision.gameObject.GetComponentInParent<Racket>();
        if (racket)
        {
            var playGameMode = Core.Instance.GetGameMode<PlayGameMode>();
            playGameMode.AddBallScore();
        }
        
        //reflect direction
        var contactPoint = collision.contacts[0].point;
        Vector2 ballLocation = _rigidbody.position;
        var hitNormal = (ballLocation - contactPoint).normalized;

        var moveDirection = _moveDirection;
        var newDirection = Vector2.Reflect(moveDirection, hitNormal);
        if (newDirection != Vector2.zero)
        {
            _moveDirection = newDirection;
        }
    }

    public Sprite GetIcon()
    {
        return ballRenderer.sprite;
    }

    public void ApplyColor(Color color)
    {
        ballRenderer.color = color;
    }
    public Color GetColor()
    {
        return ballRenderer.color;
    }
}
