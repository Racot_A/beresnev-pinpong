﻿using UnityEngine;

public class Racket : MonoBehaviour
{
    [SerializeField] private RacketRepresentation representation;
    [SerializeField] private Vector2 minMaxXPos = new Vector2(-4.3f, 4.3f);
    
    private float _moveSpeed;
    private Vector2 _moveDirection;
    private Rigidbody2D _rigidbody;

    private void Awake()
    {
        InitComponents();
    }
    private void FixedUpdate()
    {
        Move();
    }

    private void InitComponents()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        
        var inputHandler = GetComponent<InputHandler>();
        //pingPongInput.actions.
        inputHandler.MoveEvent += InputHandlerOnMoveEvent;
        
        _moveSpeed = representation.DefaultSpeed;
    }

    private void Move()
    {
        _rigidbody.velocity = _moveDirection * _moveSpeed;

        if (_rigidbody.position.x > minMaxXPos.y)
        {
            _rigidbody.position = new Vector2(minMaxXPos.y, _rigidbody.position.y);
        }
        else if (_rigidbody.position.x < minMaxXPos.x)
        {
            _rigidbody.position = new Vector2(minMaxXPos.x, _rigidbody.position.y);
        }
    }

    private void InputHandlerOnMoveEvent(Vector2 value)
    {
        _moveDirection = value;
        _moveDirection.y = 0;
    }
}
