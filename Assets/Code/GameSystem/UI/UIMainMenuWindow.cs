﻿using UnityEngine;
using UnityEngine.UI;

public class UIMainMenuWindow : MonoBehaviour
{
    [SerializeField] private Button startGameButton;
    [SerializeField] private Button quitButton;
    // Start is called before the first frame update
    void Start()
    {
        startGameButton.onClick.AddListener(LoadGame);
        quitButton.onClick.AddListener(QuitGame);
    }

    private void LoadGame()
    {
        var mainMenuGameMode = Core.Instance.GetGameMode<MainMenuGameMode>();
        mainMenuGameMode.LoadGame();
    }

    private void QuitGame()
    {
        var mainMenuGameMode = Core.Instance.GetGameMode<MainMenuGameMode>();
        mainMenuGameMode.QuitGame();
    }
}
