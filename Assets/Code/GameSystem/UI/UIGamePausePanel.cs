﻿using UnityEngine;
using UnityEngine.UI;

public class UIGamePausePanel : MonoBehaviour
{
    [SerializeField] private Button resumeGameButton;
    [SerializeField] private Button quitButton;
    [SerializeField] private UIBallCustomizationPanel customizationPanel;
    public bool IsShowed { get; private set; }

    private void Awake()
    {
        resumeGameButton.onClick.AddListener(ResumeGameClick);
        quitButton.onClick.AddListener(QuitClick);

        IsShowed = gameObject.activeInHierarchy;
    }

    private void ResumeGameClick()
    {
        var playGameMode = Core.Instance.GetGameMode<PlayGameMode>();
        playGameMode.ResumeGame();
        
        Hide();

    }
    private void QuitClick()
    {
        Core.Instance.ScenesLoadingSystem.SwitchToScene(ScenesConstants.MainMenuSceneName);
    }

    public void Show()
    {
        IsShowed = true;

        gameObject.SetActive(true);
        
        customizationPanel.Show();
    }

    public void Hide()
    {
        IsShowed = false;
        
        gameObject.SetActive(false);
        
        customizationPanel.Hide();
    }

    public void Toogle()
    {
        if (IsShowed)
        {
            Hide();
        }
        else
        {
            Show();
        }
    }
}
