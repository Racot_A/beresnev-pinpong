﻿using UnityEngine;
using UnityEngine.UI;

public class UIScorePanel : MonoBehaviour
{
    [SerializeField] private Text scoreValueText;
    [SerializeField] private Text bestScoreValueText;
    private void Start()
    {
        Core.Instance.ScoreSystem.ScoreUpdate += UpdateScore;
        Core.Instance.ScoreSystem.BestScoreUpdate += UpdateBestScore;

        UpdateScore(Core.Instance.ScoreSystem.Score);
        UpdateBestScore(Core.Instance.ScoreSystem.BestScore);
    }

    private void OnDestroy()
    {
        Core.Instance.ScoreSystem.ScoreUpdate -= UpdateScore;
        Core.Instance.ScoreSystem.BestScoreUpdate -= UpdateBestScore;
    }


    private void UpdateScore(int score)
    {
        scoreValueText.text = $"{score}";
    }
    
    private void UpdateBestScore(int bestScore)
    {
        bestScoreValueText.text = $"{bestScore}";
    }
}
