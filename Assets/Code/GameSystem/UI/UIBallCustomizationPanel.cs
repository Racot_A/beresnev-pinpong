﻿using UnityEngine;
using UnityEngine.UI;

public class UIBallCustomizationPanel : MonoBehaviour
{
    [SerializeField] private Image ballIconImage;
    [SerializeField] private Button colorButtonsPrefab;
    [SerializeField] private RectTransform colorButtonsContainer;

    public void Show()
    {
        UpdateContent();
        
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    private void UpdateContent()
    {
        var playGameMode = Core.Instance.GetGameMode<PlayGameMode>();
        var currentBall = playGameMode.CurrentBall;
        
        ballIconImage.sprite = currentBall.GetIcon();
        ballIconImage.color = Core.Instance.BallCustomizationSystem.GetBallColor(currentBall.Representation.Id);

        foreach (Transform child in colorButtonsContainer.transform)
        {
            var button = child.GetComponent<Button>();
            button.onClick.RemoveAllListeners();
            Destroy(child.gameObject);
        }

        foreach (var availableColor in Core.Instance.BallCustomizationSystem.CustomizationSettings.AvailableColors)
        {
            var colorButton = Instantiate(colorButtonsPrefab, colorButtonsContainer);
            colorButton.image.color = availableColor;
            colorButton.onClick.AddListener(() => { ClickOnColor(colorButton.image.color);});
        }
    }

    private void ClickOnColor(Color imageColor)
    {
        var playGameMode = Core.Instance.GetGameMode<PlayGameMode>();
        var currentBall = playGameMode.CurrentBall;
        
        currentBall.ApplyColor(imageColor);
        Core.Instance.BallCustomizationSystem.SetBallColor(currentBall.Representation.Id, imageColor);
        ballIconImage.color = imageColor;
    }
}
