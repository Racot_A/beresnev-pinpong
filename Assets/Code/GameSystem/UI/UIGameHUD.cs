﻿using UnityEngine;
using UnityEngine.UI;

public class UIGameHUD : MonoBehaviour
{
    [SerializeField] private Canvas canvas;

    [SerializeField] private Button pauseButton;
    [SerializeField] private UIGamePausePanel pausePanel;

    private void Awake()
    {
        pauseButton.onClick.AddListener(PauseButtonClick);
    }

    private void PauseButtonClick()
    {
        var playGameMode = Core.Instance.GetGameMode<PlayGameMode>();
        playGameMode.PauseGame();
        pausePanel.Show();
    }

    public void Show()
    {
        canvas.enabled = false;
    }

    public void Hide()
    {
        canvas.enabled = true;
    }
}
