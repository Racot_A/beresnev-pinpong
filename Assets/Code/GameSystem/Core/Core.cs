﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Core : MonoBehaviour
{
    public static Core Instance;
    public IScenesLoadingSystem ScenesLoadingSystem { get; private set; }
    public ScoreSystem ScoreSystem { get; private set; }
    public ISaveSystem SaveSystem { get; private set; }
    public BallCustomizationSystem BallCustomizationSystem { get; private set; }
    
    private IGameMode _gameMode;

    [RuntimeInitializeOnLoadMethod]
    public static void Init()
    {
        Instance = Instantiate(Resources.Load<Core>("Core"));//Initialize himself from Resorces for start game from any scene
        DontDestroyOnLoad(Instance.gameObject); 
    }

    private void Awake()
    {
        InitializeSystems();
    }
    private void Start()
    {
        TryLoadMenuScene();
    }

    private void InitializeSystems()
    {
        ScenesLoadingSystem = GetComponentInChildren<IScenesLoadingSystem>();
        ScoreSystem = GetComponentInChildren<ScoreSystem>();
        SaveSystem = GetComponentInChildren<ISaveSystem>();
        
        BallCustomizationSystem = GetComponentInChildren<BallCustomizationSystem>();
    }

    private void TryLoadMenuScene()
    {
        if (ScenesLoadingSystem.CurrentSceneIndex <= 0)
        {
            string path = SceneUtility.GetScenePathByBuildIndex(1);
            string sceneName = path.Substring(0, path.Length - 6).Substring(path.LastIndexOf('/') + 1);
            ScenesLoadingSystem.SwitchToScene(sceneName);
        }
    }

    public void AssignGameMode(IGameMode gameMode)
    {
        _gameMode = gameMode;
    }

    public T GetGameMode<T>() where T : class, IGameMode
    {
        return _gameMode as T;
    }
}
