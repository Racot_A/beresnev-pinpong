﻿using UnityEngine;

public class MainMenuGameMode : MonoBehaviour, IGameMode
{
    private void Start()
    {
        AssignToCore();
        ResumeGame();
    }
    
    public void AssignToCore()
    {
        Core.Instance.AssignGameMode(this);
    }

    public void LoadGame()
    {
        Core.Instance.ScenesLoadingSystem.SwitchToScene(ScenesConstants.GameSceneName);
    }

    public void QuitGame()
    {
        #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        #else
        Application.Quit();
        #endif
    }
    
    public void ResumeGame()
    {
        Time.timeScale = 1;
    }
}
