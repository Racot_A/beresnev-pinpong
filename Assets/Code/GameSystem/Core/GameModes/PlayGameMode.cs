﻿using UnityEngine;

public class PlayGameMode : MonoBehaviour, IGameMode
{
    [SerializeField] private int touchRacketScore = 5;
    [SerializeField] private GameField gameField;
    [SerializeField] private BallsSpawner ballsSpawner;

    private Ball _currentBall;

    public Ball CurrentBall => _currentBall;

    private void Start()
    {
        AssignToCore();
        ResumeGame();
        
        Core.Instance.SaveSystem.LoadData();

        StartGame();
    }
    
    public void AssignToCore()
    {
        Core.Instance.AssignGameMode(this);
    }

    private void StartGame()
    {
        Core.Instance.SaveSystem.LoadData();
        Core.Instance.ScoreSystem.LoadScore();
        Core.Instance.BallCustomizationSystem.LoadData();
        

        
        _currentBall = ballsSpawner.GetRandomBall();
        _currentBall.ResetToPosition(gameField.GetCenter());
        
        _currentBall.ApplyColor(Core.Instance.BallCustomizationSystem.GetBallColor(_currentBall.Representation.Id));
        
        _currentBall.gameObject.SetActive(true);
        _currentBall.StartMove();
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
    }

    public void RestartGame()
    {
        Core.Instance.SaveSystem.SaveData();
        
        Core.Instance.ScoreSystem.Score = 0;
        if (_currentBall)
        {
            _currentBall.gameObject.SetActive(false);
        }
        _currentBall = ballsSpawner.GetRandomBall();
        _currentBall.ResetToPosition(gameField.GetCenter());
        
        _currentBall.ApplyColor(Core.Instance.BallCustomizationSystem.GetBallColor(_currentBall.Representation.Id));
        _currentBall.gameObject.SetActive(true);
        _currentBall.StartMove();
    }

    public void AddBallScore()
    {
        Core.Instance.ScoreSystem.Score += touchRacketScore;
    }
}
