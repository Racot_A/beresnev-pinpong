﻿public interface IScenesLoadingSystem
{
    string CurrentScene { get; }
    int CurrentSceneIndex { get; }
    bool InLoadingProcess { get; }
    
    void SwitchToScene(string sceneName);
}
