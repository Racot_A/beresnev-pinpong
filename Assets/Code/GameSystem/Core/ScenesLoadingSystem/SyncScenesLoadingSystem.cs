﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SyncScenesLoadingSystem : MonoBehaviour
{
    public string CurrentScene => SceneManager.GetActiveScene().name;
    public int CurrentSceneIndex => SceneManager.GetActiveScene().buildIndex;
    
    public bool InLoadingProcess => _inLoading;
    
    private string _lastLoadedSceneName;
    private bool _inLoading;

    public void SwitchToScene(string sceneName)
    {
        if(_inLoading) { return; }

        Core.Instance.StartCoroutine(SwitchSceneCoroutine(sceneName, LoadSceneMode.Additive));
    }

    IEnumerator SwitchSceneCoroutine(string sceneName, LoadSceneMode loadSceneMode)
    {
        _inLoading = true;
        yield return new WaitForEndOfFrame();
        
        SceneManager.LoadScene(sceneName, loadSceneMode);
        
        if (!string.IsNullOrEmpty(_lastLoadedSceneName))
        {
            SceneManager.UnloadSceneAsync(_lastLoadedSceneName);
        }
        else
        {
            SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().name);
        }

        _lastLoadedSceneName = sceneName;

        _inLoading = false;
    }
}
