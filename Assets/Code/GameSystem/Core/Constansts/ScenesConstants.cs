﻿public class ScenesConstants
{
    public static readonly string MainMenuSceneName = "MainMenu_Scene";
    public static readonly string GameSceneName = "Game_Scene";
}
