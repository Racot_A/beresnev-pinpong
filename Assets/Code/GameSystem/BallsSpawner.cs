﻿using System.Collections.Generic;
using UnityEngine;

public class BallsSpawner : MonoBehaviour
{
   [SerializeField] private List<Ball> availableBallsPrefabs = new List<Ball>();
   [SerializeField] private Transform ballsContainerTransform;
   
   [SerializeField] private List<Ball> _balls = new List<Ball>();

   private void Awake()
   {
      CreateBalls();
   }

   private void CreateBalls()
   {
      foreach (var availableBallsPrefab in availableBallsPrefabs)
      {
         var ball = Instantiate(availableBallsPrefab, ballsContainerTransform);
         ball.gameObject.SetActive(false);
         _balls.Add(ball);
      }
   }

   public Ball GetRandomBall()
   {
      var freeBalls = _balls.FindAll(x => !x.gameObject.activeInHierarchy);
      return freeBalls[UnityEngine.Random.Range(0, freeBalls.Count)];
   }
}
